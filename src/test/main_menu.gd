extends Control


func _on_StartServer_button_up() -> void:
	var port := int($H/Server/Port.text)
	var max_players := int($H/Server/MaxPlayers.text)

	Server.start_server(port, max_players)


func _on_StartClient_button_up() -> void:
	var username := str($H/Client/Username.text)
	var port := 50301
	var input := str($H/Client/IPPort.text)
	var ip: String
	if ":" in input:
		var s = input.split(":", true, 1)
		if s[1].is_valid_integer():
			port = int(s[1])
			ip = s[0]

	Client.start_client(ip, port, username)
