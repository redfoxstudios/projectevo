extends Node
class_name InputSerialiser


const bool_inputs := ["left", "right", "up", "down", "primary", "secondary", "walk"]


static func serialise_inputs(mouse_position: Vector2) -> PoolByteArray:
	var stream := StreamPeerBuffer.new()

	var flags := 0
	for i in bool_inputs.size():
		var pot := pow(2, i) as int
		var input_name := bool_inputs[i] as String
		if Input.is_action_pressed(input_name):
			flags |= pot

	stream.put_8(flags)
	stream.put_float(mouse_position.x)
	stream.put_float(mouse_position.y)

	return stream.data_array


static func deserialise_inputs(byte_array: PoolByteArray) -> Dictionary:
	var stream := StreamPeerBuffer.new()
	stream.put_data(byte_array)
	stream.seek(0)

	var inputs := Dictionary()

	var flags := stream.get_8()

	for i in bool_inputs.size():
		var pot := pow(2, i) as int
		var input_name := bool_inputs[i] as String
		inputs[input_name] = flags & pot > 0

	inputs["mouse_position"] = Vector2(stream.get_float(), stream.get_float())

	return inputs
