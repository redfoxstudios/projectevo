extends Node


var auth_info = {
	"username": "null"
}

var network := NetworkedMultiplayerENet.new()

var blob_syncer := ClientBlobSyncer.new()


func start_client(ip: String, port: int, username: String) -> void:
	network.create_client(ip, port)
	get_tree().set_network_peer(network)

	network.connect("connection_failed", self, "_on_connection_failed")
	network.connect("connection_succeeded", self, "_on_connection_succeeded")
	network.connect("server_disconnected", self, "_on_server_disconnected")

	auth_info.username = username


func _on_connection_succeeded() -> void:
	get_tree().change_scene("res://src/test/world.tscn")
	print("Successfully connected to server")

	add_child(InputReader.new())
	add_child(blob_syncer)


func _on_connection_failed() -> void:
	print("Failed to connect to the server")


func _on_server_disconnected() -> void:
	print("Disconnected from the server")


func is_connected_to_server() -> bool:
	# Still true until one tick after server_disconnected
	return network.get_connection_status() == network.CONNECTION_CONNECTED


puppet func request_auth_info() -> void:
	Server.rpc_id(1, "return_auth_info", auth_info)


puppet func create_blob(filepath: String, blob_info: Dictionary) -> void:
	var new_blob := load(filepath).instance() as Blob
	new_blob.set_info(blob_info)
	new_blob.name = str(blob_info["id"])
	get_node("/root/World/Blobs").add_child(new_blob, true)

	Game.emit_signal("blob_spawn", new_blob)


puppet func receive_blob_states(blob_states: Dictionary, timestamp: int) -> void:
	blob_syncer.add_new_blob_states(blob_states, timestamp)
