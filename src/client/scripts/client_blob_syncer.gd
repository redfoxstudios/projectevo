extends Node
class_name ClientBlobSyncer


var blob_states_collection := [{}]


func _physics_process(_delta: float) -> void:
	_sync_blob_states()


func _sync_blob_states() -> void:
	for blob_id in blob_states_collection[0].keys():
		if str(blob_id) == "time":
			continue

		var blob := Game.get_blob_by_id(blob_id) as Blob
		if not is_instance_valid(blob):
			continue

		var state := blob_states_collection[0][blob_id] as Dictionary
		blob.interpolate_state(state, state, 1)


func add_new_blob_states(blob_states: Dictionary, timestamp: int) -> void:
	if timestamp > get_last_blob_state_time():
		blob_states["time"] = timestamp
		blob_states_collection[0] = blob_states


func get_last_blob_state_time() -> int:
	if (blob_states_collection.size() > 0
	and blob_states_collection[0].has("time")):
			return blob_states_collection[0]["time"]
	return 0

