extends Node
class_name InputReader


func _physics_process(_delta: float) -> void:
	if Client.is_connected_to_server():
		var mouse_pos := get_node("/root/World").get_global_mouse_position() as Vector2
		var serialised_inputs := InputSerialiser.serialise_inputs(mouse_pos)
		_send_inputs_to_server(serialised_inputs)
		_add_local_inputs(serialised_inputs)


func _send_inputs_to_server(serialised_inputs: PoolByteArray) -> void:
	var timestamp := OS.get_system_time_msecs()
	Server.rpc_unreliable_id(1, "receive_inputs", timestamp, serialised_inputs)


func _add_local_inputs(serialised_inputs: PoolByteArray) -> void:
	var my_net_id := get_tree().get_network_unique_id()
	var deserialised_inputs := InputSerialiser.deserialise_inputs(serialised_inputs)
	var timestamp := OS.get_system_time_msecs()
	deserialised_inputs["time"] = timestamp
	NetworkedInput.add_inputs(my_net_id, deserialised_inputs)
