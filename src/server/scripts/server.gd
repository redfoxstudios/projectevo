extends Node


var network := NetworkedMultiplayerENet.new()


func start_server(port: int, max_players: int) -> void:
	network.create_server(port, max_players)
	get_tree().set_network_peer(network)
	print("Server started")
	print("Listening on port %s" % port)

	network.connect("peer_connected", self, "_on_peer_connected")
	network.connect("peer_disconnected", self, "_on_peer_disconnected")

	get_tree().change_scene("res://src/test/world.tscn")

	add_child(ServerBlobSyncer.new())


func _on_peer_connected(player_id: int) -> void:
	var player_ip := network.get_peer_address(player_id)

	# "Catch up" player with all currently connected players
	for player in Game.get_players():
		var player_info := player.get_info() as Dictionary
		Game.rpc_id(player_id, "register_new_player", player_info)

	# Catch up with all blobs as well
	for blob in Game.get_blobs():
		var filepath := blob.filename as String
		var blob_info := blob.get_info() as Dictionary
		Client.rpc_id(player_id, "create_blob", filepath, blob_info)

	print("Player connected with id %s (%s)" % [player_id, player_ip])
	Client.rpc_id(player_id, "request_auth_info")


func _on_peer_disconnected(player_id: int) -> void:
	var player_ip := network.get_peer_address(player_id)
	print("Player disconnected with id %s (%s)" % [player_id, player_ip])

	var player := Game.get_player_by_id(player_id) as Player
	var player_blob := player.get_blob()
	if is_instance_valid(player_blob):
		delete_blob(player_blob.get_id())
	Game.rpc("delete_player", player_id)


master func return_auth_info(auth_info: Dictionary) -> void:
	var player_id := get_tree().get_rpc_sender_id()
	#var username: String = auth_info.username
	# Remove bad words from username

	if is_instance_valid(Game.get_player_by_id(player_id)):
		# Player already exists
		return

	var new_player_info := auth_info.duplicate()
	new_player_info["id"] = player_id
	new_player_info["new"] = true
	# Remove keys, login, whatever

	Game.rpc("register_new_player", new_player_info)


func create_blob(filepath: String, blob_info: Dictionary) -> Blob:
	assert(Game.is_server(), "Can't spawn blobs on client")

	var new_blob := load(filepath).instance() as Blob
	blob_info["id"] = new_blob.get_instance_id()
	new_blob.set_info(blob_info)
	new_blob.name = str(blob_info["id"])
	get_node("/root/World/Blobs").add_child(new_blob, true)
	Client.rpc_id(0, "create_blob", filepath, blob_info)
	return new_blob


func delete_blob(blob_id: int) -> void:
	assert(Game.is_server(), "Can't delete blobs on client")
	Game.rpc("delete_blob", blob_id)


master func receive_inputs(timestamp: int, serialised_inputs: PoolByteArray) -> void:
	var sender_id := get_tree().get_rpc_sender_id()
	var deserialised_inputs := InputSerialiser.deserialise_inputs(serialised_inputs)
	deserialised_inputs["time"] = timestamp
	NetworkedInput.add_inputs(sender_id, deserialised_inputs)
