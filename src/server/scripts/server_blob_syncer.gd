extends Node
class_name ServerBlobSyncer


func _physics_process(_delta: float) -> void:
	_send_blob_states()


func _send_blob_states() -> void:
	var blob_states := Dictionary()

	for blob in Game.get_blobs():
		var state := blob.get_sync_state() as Dictionary
		var id := blob.get_id() as int
		blob_states[id] = state
	
	var timestamp := OS.get_system_time_msecs()
	Client.rpc_unreliable_id(0, "receive_blob_states", blob_states, timestamp)


