extends Rule


func _ready() -> void:
	pass


func _on_player_joined(player: Player) -> void:
	if Game.is_server():
		var new_blob := Server.create_blob("res://src/shared/entities/player/player.tscn", {})
		Game.rpc("set_blob_owner", new_blob.id, player.id)

