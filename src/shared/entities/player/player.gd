extends Blob


func _physics_process(_delta: float) -> void:
	if Game.is_server():
		NetworkedInput.set_target(get_player_id())
		var input_direction := NetworkedInput.get_vector("left", "right", "up", "down")
		move_and_slide(input_direction * 2000)
		look_at(NetworkedInput.get_global_mouse_position())
