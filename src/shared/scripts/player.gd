extends Node
class_name Player

var username: String
var id: int setget ,get_id
var blob_id: int setget set_blob_id, get_blob_id

func get_info() -> Dictionary:
	return {
		"username": username,
		"id": id,
		"new": false,
	}


func set_info(info: Dictionary) -> void:
	username = info.username
	id = info.id


func get_blob() -> Node:
	return Game.get_blob_by_id(blob_id)


func has_blob() -> bool:
	return is_instance_valid(get_blob())


func get_blob_id() -> int:
	return blob_id


func get_id() -> int:
	return id


func set_blob_id(new_blob_id: int) -> void:
	blob_id = new_blob_id
