extends Node


signal player_joined(player)
signal player_left(player)
# warning-ignore:unused_signal
signal blob_spawn(blob) # emitted by Client::create_blob
signal blob_death(blob)

var players: Array

var gamemode: Gamemode setget ,get_gamemode


puppetsync func set_blob_owner(blob_id: int, player_id: int) -> void:
	var player := Game.get_player_by_id(player_id) as Player
	if is_instance_valid(player):
		var current_player_blob := player.get_blob() as Blob
		if is_instance_valid(current_player_blob):
			current_player_blob.player_id = -1

		player.set_blob_id(blob_id)

	var blob := Game.get_blob_by_id(blob_id) as Blob
	if is_instance_valid(blob):
		var current_blob_player := blob.get_player() as Player
		if is_instance_valid(current_blob_player):
			current_blob_player.blob_id = -1

		blob.set_player_id(player_id)


puppetsync func register_new_player(player_info: Dictionary) -> void:
	var new_player:= Player.new()
	new_player.set_info(player_info)
	players.push_back(new_player)

	if player_info["new"]:
		emit_signal("player_joined", new_player)


puppetsync func delete_player(player_id: int) -> void:
	var player := get_player_by_id(player_id)

	if not is_instance_valid(player):
		push_warning("Received call to delete player that doesn't exist with id %s" % player_id)
		return

	player.queue_free()
	players.erase(player)
	emit_signal("player_left", player)


puppetsync func delete_blob(blob_id: int) -> void:
	var blob := get_blob_by_id(blob_id)

	if not is_instance_valid(blob):
		push_warning("Received call to delete blob that doesn't exist")
		return

	blob.queue_free()
	emit_signal("blob_death", blob)


func load_gamemode(new_gamemode_path: String) -> void:
	var new_gamemode := load(new_gamemode_path).duplicate()
	if is_client():
		for client_rule in new_gamemode.client_scripts:
			add_script(client_rule)
	if is_server():
		for server_rule in new_gamemode.server_scripts:
			add_script(server_rule)
	for common_rule in new_gamemode.common_scripts:
		add_script(common_rule)
	gamemode = new_gamemode


func add_script(script) -> void:
	add_child(script.new())


#---------------------------------------------------------#
#  Start generic helper functions                         #
#---------------------------------------------------------#
func is_server() -> bool:
	return get_tree().is_network_server()


func is_client() -> bool:
	return not get_tree().is_network_server()


func get_gamemode() -> Gamemode:
	return gamemode as Gamemode

#---------------------------------------------------------#
#  End generic helper functions                           #
#---------------------------------------------------------#

#---------------------------------------------------------#
#  Start player helper functions                          #
#---------------------------------------------------------#
func get_player_by_id(player_id: int) -> Player:
	for player in players:
		if player.id == player_id:
			return player
	return null


func get_players() -> Array:
	return players


func get_players_count() -> int:
	return players.size()

#---------------------------------------------------------#
#  End player helper functions                            #
#---------------------------------------------------------#

#---------------------------------------------------------#
#  Start blob helper functions                            #
#---------------------------------------------------------#
func get_blob_by_id(blob_id: int) -> Blob:
	# on the server, blob's ids are the same as their instance ids
	if is_server():
		return instance_from_id(blob_id) as Blob

	var blobs := get_blobs()
	for blob in blobs:
		if blob.id == blob_id:
			return blob
	return null


func get_blobs() -> Array:
	if has_node("/root/World/Blobs"):
		return get_node("/root/World/Blobs").get_children()
	return []


func get_blobs_count() -> int:
	return get_blobs().size()

#---------------------------------------------------------#
#  End blob helper functions                            #
#---------------------------------------------------------#

