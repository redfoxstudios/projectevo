extends KinematicBody2D
class_name Blob


var id: int
var player_controller_id: int


func get_info() -> Dictionary:
	return {
		"id": id,
	}


func set_info(info: Dictionary) -> void:
	id = info["id"]


func get_player_id() -> int:
	return player_controller_id


func has_player() -> bool:
	return is_instance_valid(get_player())


func get_player() -> Player:
	return Game.get_player_by_id(player_controller_id)


func is_my_player() -> bool:
	return get_tree().get_network_unique_id() == player_controller_id


func set_player_id(player_id: int) -> void:
	player_controller_id = player_id


func get_id() -> int:
	return id


func get_sync_state() -> Dictionary:
	return {
		"p": position,
		"r": rotation,
	}


func interpolate_state(state_1: Dictionary, state_2: Dictionary, weight: float) -> void:
	position = lerp(state_1["p"], state_2["p"], weight)
	rotation = lerp_angle(state_1["r"], state_2["r"], weight)
