extends Node

"""
Format of client inputs:
key: net id of client
value:
	[
		dictionary of their latest inputs,
		dictionary of their second latest inputs
	]

"""
var client_inputs: Dictionary

var target: int setget set_target

const max_inputs_collection_size := 2


func set_target(new_target: int) -> void:
	target = new_target


func add_inputs(client_id: int, new_inputs: Dictionary) -> void:
	if not client_inputs.has(client_id):
		client_inputs[client_id] = [new_inputs]
		return

	for i in client_inputs[client_id].size():
		var input := client_inputs[client_id][i] as Dictionary
		if new_inputs["time"] > input["time"]:
			client_inputs[client_id].insert(i, new_inputs)

			# Get rid of old inputs that have been just replaced
			while client_inputs[client_id].size() > max_inputs_collection_size:
				client_inputs[client_id].pop_back()
			return

	# If wasn't later than the other inputs but can still fit in
	# client's inputs collection, add it
	if client_inputs[client_id].size() < max_inputs_collection_size:
		client_inputs[client_id].push_back(new_inputs)


func is_action_pressed(action: String) -> bool:
	if not target_has_inputs_and_property(action):
		return false
	return client_inputs[target][0][action]


func get_axis(negative_action: String, positive_action: String) -> int:
	return int(is_action_pressed(positive_action)) - int(is_action_pressed(negative_action))


func get_vector(negative_x: String, positive_x: String, negative_y: String, positive_y: String) -> Vector2:
	return Vector2(
		int(is_action_pressed(positive_x)) - int(is_action_pressed(negative_x)),
		int(is_action_pressed(positive_y)) - int(is_action_pressed(negative_y))
	).normalized()


func target_has_inputs_and_property(property: String) -> bool:
	assert(target, "Set a target client before using InputHelper functions")

	var target_input_collection := client_inputs[target] as Array
	if target_input_collection.size() == 0:
		return false

	var latest_inputs := target_input_collection.front() as Dictionary
	if not latest_inputs.has(property):
		return false

	return true


func get_global_mouse_position() -> Vector2:
	if not target_has_inputs_and_property("mouse_position"):
		return Vector2.ZERO
	return client_inputs[target][0]["mouse_position"]
