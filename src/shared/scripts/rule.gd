extends Node
class_name Rule


func _ready() -> void:
	Game.connect("player_joined", self, "_on_player_joined")
	Game.connect("player_left", self, "_on_player_left")
	Game.connect("blob_spawn", self, "_on_blob_spawned")
	Game.connect("blob_death", self, "_on_blob_died")


func _on_player_joined(_player: Player) -> void:
	pass


func _on_player_left(_player: Player) -> void:
	pass


func _on_blob_spawned(_blob: Blob) -> void:
	pass


func _on_blob_died(_blob: Blob) -> void:
	pass
