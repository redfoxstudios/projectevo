extends Resource
class_name Gamemode

export var gamemode_name: String
export var gamemode_description: String

export var client_scripts: Array
export var server_scripts: Array
export var common_scripts: Array
